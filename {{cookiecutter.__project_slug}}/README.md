# {{cookiecutter.project_name}}

<div style="display: flex; flex-direction: row; align-items: center;">

<a href="{{cookiecutter.repo_url}}">
    <img alt="pipeline status" src="{{cookiecutter.repo_url}}/badges/main/pipeline.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="{{cookiecutter.repo_url}}/-/releases">
    <img alt="latest release" src="{{cookiecutter.repo_url}}/-/badges/release.svg?order_by=release_at">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="{{cookiecutter.repo_url}}">
    <img alt="latest release" src="{{cookiecutter.repo_url}}/badges/main/coverage.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="https://github.com/pypa/hatch">
    <img alt="hatch" src="https://img.shields.io/badge/%F0%9F%A5%9A-Hatch-4051b5.svg">
</a>
<span style="display:inline-block; width: 1em;"></span>

<a href="https://github.com/psf/black">
    <img alt="black" src="https://img.shields.io/badge/code%20style-black-000000.svg">
</a>
</div>

{{cookiecutter.description}}

## Links

- Documentation : {{cookiecutter.docs_url}} 
- Issues : {{cookiecutter.issues_url}} 



