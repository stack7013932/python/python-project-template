import subprocess

GITLAB_PATH = "{{cookiecutter.gitlab_path}}"

def initialize_git_repo():
    """Initializes git repo from gitlab."""

    print("Initializing git repository")
    subprocess.call(["git", "init", "--initial-branch=main"])

    print("Adding remote origin")
    subprocess.call(["git", "remote", "add", "origin", f"git@gitlab.com:{GITLAB_PATH}.git"])

def create_hatch_envs():
    """Creates virtualenvs."""

    print("Creating default hatch env")
    subprocess.call(["hatch", "env", "create"])

    print("Creating linting hatch env")
    subprocess.call(["hatch", "env", "create", "lint"])

    print("Creating docs hatch env")
    subprocess.call(["hatch", "env", "create", "docs"])

def installing_pre_commit_in_default_env():
    """Installing pre-commit"""

    print("Installing pre-commit")
    subprocess.call(["hatch", "run", "pre-commit", "install"])

initialize_git_repo()
create_hatch_envs()
installing_pre_commit_in_default_env()